#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <vector>

#define PIXEL_MAX 255
#define PIXEL_MIN 0

int correctUint8PixelValue(int value) {
    int newValue = value;
    if (newValue > PIXEL_MAX) {
        newValue = PIXEL_MAX;
    }
    else if (newValue < PIXEL_MIN) {
        newValue = PIXEL_MIN;
    }
    return newValue;
}

cv::Mat& thresholdRGB(const cv::Mat& input, cv::Mat& output, const int rMin, const int rMax, const int gMin, const int gMax, const int bMin, const int bMax) {
	CV_Assert(input.depth() != sizeof(uchar));
	CV_Assert(input.channels() == 3);

	cv::Mat_<cv::Vec3b> _I = input;
	cv::Mat_<cv::Vec3b> _O = output;

	for( int i = 0; i < input.rows; ++i)
		for( int j = 0; j < input.cols; ++j ){
            if (_I(i, j)[0] >= rMin && _I(i, j)[0] <= rMax &&
                _I(i, j)[1] >= gMin && _I(i, j)[1] <= gMax &&
                _I(i, j)[2] >= bMin && _I(i, j)[2] <= bMax) {

                _O(i, j)[0] = _I(i, j)[0];
                _O(i, j)[1] = _I(i, j)[1];
                _O(i, j)[2] = _I(i, j)[2];
			}
            else {

                _O(i, j)[0] = 0;
                _O(i, j)[1] = 0;
                _O(i, j)[2] = 0;
            }
		}
	return output;
}

void modifyPixelChannelIntensity(const uchar* src, uchar* dst, const int intensityModification) {
    int newValue = *src + intensityModification;
    newValue = correctUint8PixelValue(newValue);
    *dst = static_cast<uchar>(newValue);
}

cv::Mat& modifyIntensity(const cv::Mat& input, cv::Mat& output, int intensityModification) {
    CV_Assert(intensityModification < 100 && intensityModification > -100);
	cv::Mat_<cv::Vec3b> _I = input;
	cv::Mat_<cv::Vec3b> _O = output;
	for (int i = _I.rows-1; i > _I.rows/2; --i){
		for (int j = _I.rows-i-1; j < _I.cols-(_I.rows-i-1); ++j) {
            modifyPixelChannelIntensity(&_I(i, j)[0], &_O(i, j)[0], intensityModification);
            modifyPixelChannelIntensity(&_I(i, j)[1], &_O(i, j)[1], intensityModification);
            modifyPixelChannelIntensity(&_I(i, j)[2], &_O(i, j)[2], intensityModification);
		}
    }
    output = _O;
    return output;
}
void modifyPixelChannelContrast(const uchar* src, uchar* dst, const float contrastMultiplier) {
    int newValue = static_cast<int>(*src * contrastMultiplier);
    newValue = correctUint8PixelValue(newValue);
    *dst = static_cast<uchar>(newValue);
}



cv::Mat& modifyContrast(const cv::Mat& input, cv::Mat& output, float contrastMultiplier) {
    CV_Assert(contrastMultiplier < 2 && contrastMultiplier > 0);
	cv::Mat_<cv::Vec3b> _I = input;
	cv::Mat_<cv::Vec3b> _O = output;
	for (int i = 0; i < _I.rows/2; ++i){
		for (int j = i; j < _I.cols - i; ++j) {
            modifyPixelChannelContrast(&_I(i, j)[0], &_O(i, j)[0], contrastMultiplier);
            modifyPixelChannelContrast(&_I(i, j)[1], &_O(i, j)[1], contrastMultiplier);
            modifyPixelChannelContrast(&_I(i, j)[2], &_O(i, j)[2], contrastMultiplier);
		}
    }
    output = _O;
    return output;
}

uchar calculateWeirdColorValue(cv::Vec3b pixel) {
    // opencv loads images as BGR, not RGB
    int B = pixel[0];
    int G = pixel[1];
    int R = pixel[2];
    int colorValue = R - static_cast<int>((R + G + B) / 3);
	colorValue = correctUint8PixelValue(colorValue);
    return static_cast<uchar>(colorValue);
}

cv::Mat& modifyColor(const cv::Mat& input, cv::Mat& output) {
	cv::Mat_<cv::Vec3b> _I = input;
	cv::Mat_<cv::Vec3b> _O = output;
    uchar colorValue;
	for (int i = 0; i < _I.cols/2; ++i){
		for (int j = i; j < _I.rows-i; ++j) {
            colorValue = calculateWeirdColorValue(_I(j, i));
            _O(j, i)[0] = colorValue;
            _O(j, i)[1] = colorValue;
            _O(j, i)[2] = colorValue;
		}
    }
    output = _O;
    return output;
}



void runIntensity() {
    cv::Mat image = cv::imread("Lena.png");
    cv::Mat result(image.rows, image.cols, image.type());
    modifyIntensity(image, result, 99);
    cv::imshow("Original",image);
    cv::imshow("Result",result);
    cv::waitKey(-1);
}

void runContrast() {
    cv::Mat image = cv::imread("Lena.png");
    cv::Mat result(image.rows, image.cols, image.type());
    modifyContrast(image, result, 1.9f);
    cv::imshow("Original",image);
    cv::imshow("Result",result);
    cv::waitKey(-1);
}

void runColorPicker() {
    cv::Mat image = cv::imread("Lena.png");
    cv::Mat result(image.rows, image.cols, image.type());
    modifyColor(image, result);
    cv::imshow("Original",image);
    cv::imshow("Result",result);
    cv::waitKey(-1);
}

int calculateHistogramBinRange(const int nBins) {
    return static_cast<int>((PIXEL_MAX+1)/nBins);
}



void addPixelToHistogram(std::vector<int>& histogramBins, int pixelValue) {
    int nBins = histogramBins.size();
    int binRange = calculateHistogramBinRange(nBins);
    for (int i = 1; i <= nBins; ++i) {
        //std::cout << "pixval=" << pixelValue << " < " << i * binRange << std::endl;
        if (pixelValue < i * binRange) {
            histogramBins[static_cast<int>(i) - 1] += 1;
            break;
        }
    }

}
std::vector<int> calculateImageHistogram(cv::Mat image, int nBins=8) {
    CV_Assert(nBins > 0);
    CV_Assert((PIXEL_MAX+1) % nBins == 0);
	cv::Mat_<cv::Vec3b> _I = image;
    auto histogramBins = std::vector<int>(nBins);
    int pixelValue;
	for (int i = 0; i < _I.rows; ++i){
		for (int j = 0; j < _I.cols; ++j) {
            //std::cout << "(" << i << ", " << j << ")" << std::endl;
            pixelValue =( _I(i, j)[0] + _I(i, j)[1] + _I(i, j)[2])/3;
            addPixelToHistogram(histogramBins, pixelValue);
		}
    }
    return histogramBins;
}

void printHistogram(const std::vector<int>& histogram) {
    int nBins = histogram.size();
    int binRange = calculateHistogramBinRange(nBins);
    for (int i = 0; i < nBins; ++i) {
        std::cout << "Intensity range: <" << i * binRange << ", " << (i + 1) * binRange - 1 << ">, " << "pixel count: " << histogram[i] << std::endl;
    }
}

void runHistogram() {
    cv::Mat image = cv::imread("Lena.png");
    auto histogram = calculateImageHistogram(image);
    printHistogram(histogram);
}

void runSolution() {
    cv::Mat image = cv::imread("Lena.png");
    cv::Mat result = image.clone();

    // modify image
    modifyIntensity(image, result, 99);
    modifyColor(image, result);
    modifyContrast(image, result, 1.3f);

    // calculate and display histogram
    auto histogram = calculateImageHistogram(result);
    printHistogram(histogram);

    // display orig and results
    cv::imshow("Original",image);
    cv::imshow("Result",result);
    //cv::imwrite("result.png", result);
    cv::waitKey(-1);

}


int main(int, char *[]) {
    //runIntensity();
    //runContrast();
    //runColorPicker();
    //runHistogram();
    runSolution();
    return 0;
}
