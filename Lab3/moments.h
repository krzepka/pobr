#pragma once

#define _USE_MATH_DEFINES

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <limits>
#include <cmath>


double calculateMomentInvariant1(const cv::Mat& figure);
double calculateMomentInvariant7(const cv::Mat& figure);
double calculateNormalMoment(const cv::Mat& figure, const int p, const int q);
