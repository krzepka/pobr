#pragma once
#define _USE_MATH_DEFINES

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <limits>
#include <cmath>


long calculateArea(const cv::Mat& figure);
bool isPixelCorner(const cv::Mat& figure, int pixelX, int pixelY);
long calculateCircumference(const cv::Mat& figure);
double calculateMalinowskaCoeff(long S, long L);