#pragma once
#define _USE_MATH_DEFINES

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <vector>
#include <limits>
#include <cmath>

#include "moments.h"
#include "shape_coeff.h"



void calculateAndDisplayArrowResults(const std::string& fname, int idx);
