#include "moments.h"

double calculateNormalMoment(const cv::Mat& figure, const int p, const int q) {
    cv::Mat_<uchar> _I = figure;
    double m = 0.0;

    for (int i = 1; i <= _I.rows; ++i) {
        for (int j = 1; j <= _I.cols; ++j) {
            if (_I(i - 1, j - 1) == 0) {
				m += pow(i, p) * pow(j, q) * 1;
            }
        }
    }
    return m;
}

double calculateCentralMoment(const cv::Mat& figure, const int p, const int q) {
    cv::Mat_<uchar> _I = figure;
    double M = 0.0;

    double m00 = calculateNormalMoment(figure, 0, 0);
    double m01 = calculateNormalMoment(figure, 0, 1);
    double m10 = calculateNormalMoment(figure, 1, 0);

    double center_x = m10 / m00;
    double center_y = m01 / m00;

    for (int i = 1; i <= _I.rows; ++i){
        for (int j = 1; j <= _I.cols; ++j) {
            if(_I(i - 1, j - 1) == 0)
            M += pow(i - center_x, p) * pow(j - center_y, q) * 1;
        }
	}
    return M;
}

double calculate_M_00(const cv::Mat& figure) {
    return calculateNormalMoment(figure, 0, 0);
}

double calculate_M_20(const cv::Mat& figure) {
    double m_00 = calculateNormalMoment(figure, 0, 0);
    double m_20 = calculateNormalMoment(figure, 2, 0);
    double m_10 = calculateNormalMoment(figure, 1, 0);
    
    return m_20 - (m_10 / m_00) * m_10;
}

double calculate_M_02(const cv::Mat& figure) {
    double m_00 = calculateNormalMoment(figure, 0, 0);
    double m_02 = calculateNormalMoment(figure, 0, 2);
    double m_01 = calculateNormalMoment(figure, 0, 1);
    
    return m_02 - (m_01 / m_00) * m_01;
}

double calculate_M_11(const cv::Mat& figure) {
    double m_00 = calculateNormalMoment(figure, 0, 0);
    double m_11 = calculateNormalMoment(figure, 1, 1);
    double m_01 = calculateNormalMoment(figure, 0, 1);
    double m_10 = calculateNormalMoment(figure, 1, 0);
    
    return m_11 - (m_01 / m_00) * m_10;
}


double calculateMomentInvariant1(const cv::Mat& figure) {
    double M_20 = calculate_M_20(figure);
    double M_02 = calculate_M_02(figure);
    double m_00 = calculateNormalMoment(figure, 0, 0);
    
    return ((M_20+M_02) / m_00) / m_00;
}

double calculateMomentInvariant7(const cv::Mat& figure) {
    double M_20 = calculate_M_20(figure);
    double M_02 = calculate_M_02(figure);
    double M_11 = calculate_M_11(figure);
    double m_00 = calculateNormalMoment(figure, 0, 0);

    return (M_20*M_02 - pow(M_11, 2)) / pow(m_00, 4);
}
