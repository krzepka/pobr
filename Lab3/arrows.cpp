#include "arrows.h"


cv::Vec2d calculateObjectMassCenter(const cv::Mat& figure) {
    double m_00 = calculateNormalMoment(figure, 0, 0);
    double m_10 = calculateNormalMoment(figure, 1, 0);
    double m_01 = calculateNormalMoment(figure, 0, 1);

    return { m_10 / m_00, m_01 / m_00 };
}

cv::Vec2d calculateObjectGeometricCenter(const cv::Mat& figure) {
    /* Calculates bounding box and then the geometric center
    */

    cv::Mat_<uchar> _I = figure;
    long long maxY = 0;
    long long minY = LLONG_MAX;
    long long maxX = 0;
    long long minX = LLONG_MAX;

    for (int i = 0; i < _I.rows; ++i) {
        for (int j = 0; j < _I.cols; ++j) {
            if (_I(i, j) == 0) {
                if (i > maxX) maxX = i;
                if (i < minX) minX = i;
                if (j > maxY) maxY = j;
                if (j < minY) minY = j;
            }
        }
    }

    return { static_cast<double>(maxX + minX) / 2.0, 
             static_cast<double>(maxY + minY) / 2.0 };
}

cv::Mat extractGrayscaleArrow(const cv::Mat& figure, cv::Mat& result, cv::Vec3b color) {
    cv::Mat_<cv::Vec3b> _I = figure;
    cv::Mat_<uchar> _O = result; 

    for (int i = 0; i < _I.rows; ++i) {
        for (int j = 0; j < _I.cols; ++j) {
            if (_I(i, j)[0] != color[0] || _I(i, j)[1] != color[1]) {
                _O(i, j) = UCHAR_MAX;
            }
            else {
                _O(i, j) = 0;
            }
        }
    }
    return _O;
}

double calculateVectorAngle(cv::Vec2d inVector) {
    /*  calculates an angle of the given 2D vector
        based on: https://stackoverflow.com/questions/14066933/direct-way-of-computing-clockwise-angle-between-2-vectors
    */
    cv::Vec2d baseVector = { 0.0, -1.0 };
    double dotProduct = inVector[0] * baseVector[0] + inVector[1] * baseVector[1];
	double determinant = inVector[0] * baseVector[1] - inVector[1] * baseVector[0]; 
    return atan2(determinant, dotProduct)*180/M_PI;
}

double calculateArrowAngle(cv::Vec2d massCenter, cv::Vec2d geoCenter) {
    cv::Vec2d arrowVector = { (massCenter[0] - geoCenter[0]), (massCenter[1] - geoCenter[1])};
    return calculateVectorAngle(arrowVector);
}


void displayArrowResults(const std::string& fname, const int R, cv::Mat& figureSingleArrow, double angle, int idx) {
	long area = calculateArea(figureSingleArrow);
	long circum = calculateCircumference(figureSingleArrow);
	double malinowska = calculateMalinowskaCoeff(area, circum);
	double m1 = calculateMomentInvariant1(figureSingleArrow);
	double m7 = calculateMomentInvariant7(figureSingleArrow);

    std::cout << idx << ". Strzalka R = " << R << " nachylenie=" << angle << " " <<fname << " S=" << area << " L=" << circum << " W3=" << malinowska << " M1=" << m1 << " M7=" << m7 <<std::endl;
}

void calculateAndDisplayArrowResults(const std::string& fname, int idx) {
    const cv::Mat figure = cv::imread(fname);
    const std::vector<cv::Vec3b> arrowColors = { {0, 180, 0}, {0, 135, 45}, {0, 90, 90}, {0, 45, 135}, {0, 0, 180} };
    std::vector<double> angles;
	cv::Mat figureSingleArrow(figure.rows, figure.cols, CV_8UC1);
	cv::Vec2d massCenter;
	cv::Vec2d geoCenter;


    for (int i = 0; i < arrowColors.size(); ++i) {
		extractGrayscaleArrow(figure, figureSingleArrow, arrowColors[i]);

		massCenter = calculateObjectMassCenter(figureSingleArrow);
		geoCenter = calculateObjectGeometricCenter(figureSingleArrow);
        angles.push_back(calculateArrowAngle(massCenter, geoCenter));

		displayArrowResults(fname, arrowColors[i][2], figureSingleArrow, angles[i], idx);

		//cv::imshow("Arrow R = "+ std::to_string(arrowColors[i][2]),figureSingleArrow);
		//cv::waitKey(-1);

		figureSingleArrow = cv::Mat(figure.rows, figure.cols, CV_8UC1);
    }
}
