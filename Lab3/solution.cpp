#define _USE_MATH_DEFINES

#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/imgproc.hpp>
#include <iostream>
#include <vector>
#include <limits>
#include <cmath>

#include "moments.h"
#include "shape_coeff.h"
#include "arrows.h"

void assertGrayscale(const cv::Mat& figure) {
	CV_Assert(figure.depth() != sizeof(uchar));
	CV_Assert(figure.channels() == 1);
}


void displayResults(const std::string& fname, long area, long circum, double malinowska, double moment1, double moment7, int idx) {
    std::cout << idx << ". Plik: " << fname << " S=" << area << " L=" << circum << " W3=" << malinowska << " M1=" << moment1 << " M7=" << moment7<<std::endl;
}


void calculateAndDisplayParameters(const std::string& fname, int idx) {
    const cv::Mat& figure = cv::imread(fname, cv::IMREAD_GRAYSCALE);
    //cv::imshow("Original", figure);
    //cv::waitKey(-1);

    assertGrayscale(figure);
    long area = calculateArea(figure);
    long circum = calculateCircumference(figure);
    double malinowska = calculateMalinowskaCoeff(area, circum);
    double m1 = calculateMomentInvariant1(figure);
    double m7 = calculateMomentInvariant7(figure);

    displayResults(fname, area, circum, malinowska, m1, m7, idx);
}

void displayArrowResults(const std::string& fname, long area, long circum, double malinowska, double moment1, double moment7, double angle, int idx) {
    std::cout << idx << ". Strzalka R = " << 0 << " nachylenie=" << angle << fname << " S=" << area << " L=" << circum << " W3=" << malinowska << " M1=" << moment1 << " M7=" << moment7<<std::endl;
}



void runSolution() {
    std::vector<std::string> fnames = { "elipsa.dib", "elipsa1.dib", "kolo.dib",  "troj.dib",  "prost.dib" };
    for (int i = 0; i < fnames.size(); ++i) {
		calculateAndDisplayParameters(fnames[i], i + 1);
    }

    std::vector<std::string> arrowFnames = {"strzalki_1.dib", "strzalki_2.dib"};
    for (int i = 0; i < arrowFnames.size(); ++i) {
        calculateAndDisplayArrowResults(arrowFnames[i], i);
    }
}


int main(int, char *[]) {
    runSolution();
    return 0;
}