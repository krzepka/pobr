#include "shape_coeff.h"


long calculateArea(const cv::Mat& figure) {
    cv::Mat_<uchar> _I = figure;
    long area = 0;

    for (int i = 0; i < _I.rows; ++i)
        for (int j = 0; j < _I.cols; ++j) {
            if (_I(i, j) == 0) {
                ++area;
            }
        }
    return area;
}

bool isPixelCorner(const cv::Mat& figure, int pixelX, int pixelY) {
    /*
    Tests if the given pixel is in the corner. Invoke for the pixel with the UCHAR_MIN (black color) value.
    Return true if any of the surrounding pixels (in the 3x3 kernel) has value UCHAR_MAX (white color).
    */
    int kernel_size = 3;
    int halfKernel = static_cast<int>(kernel_size / 2);
    int lowX = MAX(pixelX - kernel_size, 0);
    int highX = MAX(pixelX + kernel_size, figure.rows);
    int lowY = MAX(pixelY - kernel_size, 0);
    int highY = MAX(pixelY + kernel_size, figure.cols);

    cv::Mat_<uchar> _I = figure;

    for (int i = lowX, i_ = 0; i <= highX && i_ < kernel_size; ++i, ++i_) {
        for (int j = lowY, j_ = 0; j <= highY && j_ < kernel_size; ++j, ++j_) {
            if (_I(i, j) == UCHAR_MAX)
                return true;
        }
    }
    return false;
}

long calculateCircumference(const cv::Mat& figure) {
    cv::Mat_<uchar> _I = figure;
    long circum = 0;

    for (int i = 0; i < _I.rows; ++i)
        for (int j = 0; j < _I.cols; ++j) {
            if (_I(i, j) == 0 && isPixelCorner(figure, i, j)) {
                ++circum;
            }
        }
    return circum;
}

double calculateMalinowskaCoeff(long S, long L) {
    return L / (2 * sqrt(M_PI * S)) - 1;
}
