#include "opencv2/core/core.hpp"
#include "opencv2/highgui/highgui.hpp"
#include <opencv2/imgproc.hpp>
#include "opencv2/imgcodecs.hpp"
#include <opencv2/core/hal/interface.h>
#include <iostream>
#include <vector>
#include <algorithm>

#define PIXEL_MAX 255
#define PIXEL_MIN 0

#define CONV_KERNEL_SIZE 5


std::vector<std::vector<float>> kernelLowpass = { {1, 1, 1, 1, 1},
										   {1, 1, 1, 1, 1},
										   {1, 1, 1, 1, 1},
										   {1, 1, 1, 1, 1},
										   {1, 1, 1, 1, 1}};

std::vector<std::vector<float>> kernelLowpass2 = { {0, 0, 0, 0, 0}, 
										   {0, 1, 1, 1, 0},
										   {0, 1, 1, 1, 0},
										   {0, 1, 1, 1, 0},
										   {0, 0, 0, 0, 0}};



// https://www.opengl.org/archives/resources/code/samples/advanced/advanced97/notes/node171.html#SECTION000143550000000000000
std::vector<std::vector<float>> kernelHighpass = { {-1, -1, -1, -1, -1},
										   {-1, -1, -1, -1, -1},
										   {-1, -1, 24, -1, -1},
										   {-1, -1, -1, -1, -1},
										   {-1, -1, -1, -1, -1}};

std::vector<std::vector<float>> kernelHighpass2 = { {0, 0, 0, 0, 0},
										   {0, -1, -1, -1, 0},
										   {0, -1, 8, -1, 0},
										   {0, -1, -1, -1, 0},
										   {0, 0, 0, 0, 0}};



void performConvolution(const cv::Mat& input, const cv::Mat& output, const std::vector<std::vector<float>>& kernel, const int pixelX, const int pixelY, float kernelSum) {
    int halfKernel = static_cast<int>(CONV_KERNEL_SIZE / 2);
    int lowX = MAX(pixelX - CONV_KERNEL_SIZE, 0);
    int highX = MAX(pixelX + CONV_KERNEL_SIZE, input.rows);
    int lowY = MAX(pixelY - CONV_KERNEL_SIZE, 0);
    int highY = MAX(pixelY + CONV_KERNEL_SIZE, input.cols);
	cv::Mat_<cv::Vec3b> _I = input;
	cv::Mat_<cv::Vec3b> _O = output;

    float valueR = 0;
    float valueG = 0;
    float valueB = 0;

    for (int i = lowX, i_ = 0; i <= highX && i_ < CONV_KERNEL_SIZE; ++i, ++i_) {
		for (int j = lowY, j_ = 0; j <= highY && j_ < CONV_KERNEL_SIZE; ++j, ++j_) {
            valueR += _I(i, j)[0]*kernel[i_][j_];
            valueG += _I(i, j)[1]*kernel[i_][j_];
            valueB += _I(i, j)[2]*kernel[i_][j_];
		}
    }
    // handle case when 
    if (kernelSum == 0) {
        kernelSum = 1;
    }
    _O(pixelX, pixelY)[0] = cv::saturate_cast<uchar>(valueR/kernelSum);
    _O(pixelX, pixelY)[1] = cv::saturate_cast<uchar>(valueG/kernelSum);
    _O(pixelX, pixelY)[2] = cv::saturate_cast<uchar>(valueB/kernelSum);


}

float calculateKernelSum(const std::vector<std::vector<float>>& kernel) {
    float kernelSum = 0;
    for (auto& n : kernel) {
        for (auto& m : n) {
            kernelSum += m;
        }
    }
    return kernelSum;
}

cv::Mat& applyConvFilter(const cv::Mat& input, cv::Mat& output, const std::vector<std::vector<float>>& kernel) {
    CV_Assert(kernel.size() == CONV_KERNEL_SIZE );
    float kernelSum = kernelSum = calculateKernelSum(kernel);

	for (int i = 0; i < input.rows; ++i){
		for (int j = 0; j < input.cols; ++j) {
            performConvolution(input, output, kernel, i, j, kernelSum);
		}
    }
    return output;
}

void rankFilterFunction(const cv::Mat& input, cv::Mat& output, int kernelSize, int pixelX, int pixelY, int filterIndex) {
    int halfKernel = static_cast<int>(CONV_KERNEL_SIZE / 2);
    int lowX = MAX(pixelX - CONV_KERNEL_SIZE, 0);
    int highX = MAX(pixelX + CONV_KERNEL_SIZE, input.rows);
    int lowY = MAX(pixelY - CONV_KERNEL_SIZE, 0);
    int highY = MAX(pixelY + CONV_KERNEL_SIZE, input.cols);
	cv::Mat_<cv::Vec3b> _I = input;
	cv::Mat_<cv::Vec3b> _O = output;

    std::vector<uchar>  kernelVectorR;
    std::vector<uchar>  kernelVectorG;
    std::vector<uchar>  kernelVectorB;

    for (int i = lowX, i_ = 0; i <= highX && i_ < CONV_KERNEL_SIZE; ++i, ++i_) {
		for (int j = lowY, j_ = 0; j <= highY && j_ < CONV_KERNEL_SIZE; ++j, ++j_) {
            kernelVectorR.push_back(_I(i, j)[0]);
            kernelVectorG.push_back(_I(i, j)[1]);
            kernelVectorB.push_back(_I(i, j)[2]);
		}
    }
    std::sort(kernelVectorR.begin(), kernelVectorR.end());
    std::sort(kernelVectorG.begin(), kernelVectorG.end());
    std::sort(kernelVectorB.begin(), kernelVectorB.end());

    _O(pixelX, pixelY)[0] = kernelVectorR[filterIndex];
    _O(pixelX, pixelY)[1] = kernelVectorG[filterIndex];
    _O(pixelX, pixelY)[2] = kernelVectorB[filterIndex];
}

cv::Mat& applyRankFilter(const cv::Mat& input, cv::Mat& output, int kernelSize, int filterIndex) {
    CV_Assert(kernelSize%2 == 1 && kernelSize > 0);
	for (int i = 0; i < input.rows; ++i){
		for (int j = 0; j < input.cols; ++j) {
            rankFilterFunction(input, output, kernelSize, i, j, filterIndex);
		}
    }
    return output;
}

void runRank(int index, std::string path) {
    cv::Mat image = cv::imread(".\\data\\Lena.bmp");
    cv::Mat result = image.clone();
    int rankKernelSize = 7;

	applyRankFilter(image, result, rankKernelSize, index);

    // display orig and results
    cv::imshow("Original",image);
    cv::imshow("Result",result);
    cv::imwrite(path, result);
    cv::waitKey(-1);
}

void runConv(std::vector<std::vector<float>> kernel, std::string path) {
    cv::Mat image = cv::imread(".\\data\\Lena.bmp");
    cv::Mat result = image.clone();

	applyConvFilter(image, result, kernel);

    // display orig and results
    cv::imshow("Original",image);
    cv::imshow("Result",result);
    cv::imwrite(path, result);
    cv::waitKey(-1);

}

void runLowpass() {
    runConv(kernelLowpass, ".\\data\\result_lowpass.png");
}

void runHighpass() {
    runConv(kernelHighpass, ".\\data\\result_highpass.png");
}

int main(int, char *[]) {
    runLowpass();
    runHighpass();
    runRank(0,  ".\\data\\result_rank0.png");
    runRank(12, ".\\data\\result_rank12.png");
    runRank(24, ".\\data\\result_rank25.png");
    return 0;
}
